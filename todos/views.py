from django.urls import reverse
from django.views.generic import CreateView, UpdateView, DeleteView
from django.shortcuts import render, get_object_or_404
from .models import TodoList

def todo_list_view(request):
    todos = TodoList.objects.all()
    context = {'todos': todos}
    return render(request, 'todos/todo_list.html', context)

def todo_list_detail_view(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    context = {
        'todo_list': todo_list,
        'todo_items': todo_list.items.all()
    }
    return render(request, 'todos/todo_list_detail.html', context)

class TodoListCreateView(CreateView):
    model = TodoList
    fields = ['name']
    template_name = 'todos/todo_list_form.html'

    def get_success_url(self):
        return reverse('todo_list_detail',
                        kwargs={'id': self.object.id})

class TodoListUpdateView(UpdateView):
    model = TodoList
    fields = ['name']
    template_name = 'todos/todo_list_update_form.html'

    def get_success_url(self):
        return reverse('todo_list_detail',
                        kwargs={'id': self.object.id}
                        )
class TodoListDeleteView(DeleteView):
    model = TodoList
    success_url = '/todos/'
    template_name = 'todos/todo_list_delete_form.html'

from django.urls import path
from todos.views import todo_list_view, todo_list_detail_view, TodoListCreateView, TodoListUpdateView, TodoListDeleteView

urlpatterns = [
    path("", todo_list_view, name="todo_list_list"),
    path("create/", TodoListCreateView.as_view(), name="todo_list_create"),  # Create view
    path("<int:id>/", todo_list_detail_view, name="todo_list_detail"),
    path("<int:id>/edit/", TodoListUpdateView.as_view(), name="todo_list_update"),  # Update view
    path("<int:id>/delete/", TodoListDeleteView.as_view(), name="todo_list_delete"),
]
